import Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Goalkeeper } from '../lib/types';
import { calculateGsaa } from '../lib/helper';

export class GsaaToWorkloadPlot {
  constructor(selector: string, data: Array<Goalkeeper>) {
    const canvas: HTMLCanvasElement | null = document.querySelector(selector);
    if (!canvas) {
      window.console.error(`no canvas with selector ${selector} found`);
      return;
    }

    const canvasContext = canvas.getContext('2d');
    new Chart(canvasContext!, {
      plugins: [ChartDataLabels],
      data: {
        datasets: [{
          type: 'scatter',
          fill: false,
          backgroundColor: 'rgba(66,133,244,0.3)',
          hoverBackgroundColor: 'rgba(66,133,244,0.3)',
          borderColor: 'rgba(66,133,244,1)',
          hoverBorderColor: 'rgba(66,133,244,1)',
          borderWidth: 1,
          pointHitRadius: 5,
          pointRadius: 5,
          pointHoverRadius: 5,
          showLine: false,
          data: data.map(e => ({
            x: 100 * calculateGsaa(e),
            y: e.playtime! / e.totalShotsAgainst
          }))
        }, {
          type: 'line',
          fill: false,
          backgroundColor: 'rgba(255,102,102,1)',
          borderColor: 'rgba(255,102,102,1)',
          borderWidth: 1,
          pointRadius: 0,
          pointHitRadius: 0,
          data: [{
            x: 0,
            y: 0
          }, {
            x: 0,
            y: Math.ceil(Math.max(...data.map(e => e.playtime! / e.totalShotsAgainst)))
          }]
        }, {
          type: 'line',
          fill: false,
          backgroundColor: 'rgba(255,102,102,1)',
          borderColor: 'rgba(255,102,102,1)',
          borderWidth: 1,
          pointRadius: 0,
          pointHitRadius: 0,
          data: [{
            x: Math.floor(100 * Math.min(...data.map(e => calculateGsaa(e))) / 5) * 5,
            y: data.map(e => e.playtime! / e.totalShotsAgainst).reduce((a, b) => a + b) / data.length
          }, {
            x: Math.ceil(100 * Math.max(...data.map(e => calculateGsaa(e))) / 5) * 5,
            y: data.map(e => e.playtime! / e.totalShotsAgainst).reduce((a, b) => a + b) / data.length
          }]
        }]
      },
      options: {
        plugins: {
          datalabels: {
            anchor: 'end',
            align: 270,
            offset: -4,
            formatter: (_, context) => {
              if (context.dataset.type === 'scatter') {
                const name = data[context.dataIndex].name.split(' ');
                return `${name[0][0]}. ${name.slice(1).join(' ')}`;
              }
              return '';
            }
          }
        },
        tooltips: {
          enabled: false
        },
        legend: {
          display: false
        },
        animation: {
          duration: 0
        },
        hover: {
          mode: 'point',
          animationDuration: 0,
        },
        responsiveAnimationDuration: 0,
        responsive: false,
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'GSAA'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Minuten pro gehaltenen Schuss'
            },
            ticks: {
              min: Math.floor(Math.min(...data.map(e => e.playtime! / e.totalShotsAgainst)))
            }
          }]
        }
      }
    });
  }
}
