import Chart, { Point } from 'chart.js';
import { sumByPointDimension, setCanvasDimensions } from '../lib/helper';

export type XgPlotData = {
  team1: {
    name: string,
    points: Array<EventInput>
  },
  team2: {
    name: string,
    points: Array<EventInput>
  }
};

type EventInput = [number, number, string?];

type LabeledPoint = Point & { label: string | undefined };

export class XgPlot {
  constructor(selector: string, data: XgPlotData) {
    const canvas: HTMLCanvasElement | null = document.querySelector(selector);
    if (!canvas) {
        window.console.error(`no canvas with selector ${selector} found`);
        return;
    }

    setCanvasDimensions(canvas, 2);

    const team1points = data.team1.points.map((e: EventInput) => ({
        x: e[0],
        y: e[1],
        label: e[2] ? e[2] : undefined
    }));
    const team2points = data.team2.points.map((e: EventInput) => ({
        x: e[0],
        y: e[1],
        label: e[2] ? e[2] : undefined
    }));

    const maxMinute = Math.max(...team1points.map(e => e.x), ...team2points.map(e => e.x)) <= 90 ? 91 : 121;

    const sumTeam1 = sumByPointDimension([{x: 0, y: 0}].concat(team1points, {x: maxMinute, y: 0}));
    const sumTeam2 = sumByPointDimension([{x: 0, y: 0}].concat(team2points, {x: maxMinute, y: 0}));

    const canvasContext = canvas.getContext('2d');
    new Chart(canvasContext!, {
      type: 'line',
      data: {
        datasets: [{
          type: 'scatter',
          backgroundColor: 'rgba(255,255,255,1)',
          hoverBackgroundColor: 'rgba(255,255,255,1)',
          borderColor: 'rgba(66,133,244,1)',
          hoverBorderColor: 'rgba(66,133,244,1)',
          borderWidth: 2,
          hoverBorderWidth: 2,
          fill: true,
          pointRadius: 5,
          pointHoverRadius: 5,
          showLine: false,
          data: sumTeam1.filter(e => e.label !== undefined).map(e => ({
              x: e.x,
              y: e.y
          }))
        }, {
          type: 'scatter',
          backgroundColor: 'rgba(255,255,255,1)',
          hoverBackgroundColor: 'rgba(255,255,255,1)',
          borderColor: 'rgba(219,68,55,1)',
          hoverBorderColor: 'rgba(219,68,55,1)',
          borderWidth: 2,
          hoverBorderWidth: 2,
          fill: true,
          pointRadius: 5,
          pointHoverRadius: 5,
          showLine: false,
          data: sumTeam2.filter(e => e.label !== undefined).map(e => ({
              x: e.x,
              y: e.y
          }))
        }, {
          label: data.team1.name,
          steppedLine: 'before',
          fill: true,
          backgroundColor: 'rgba(66,133,244,0.3)',
          borderColor: 'rgba(66,133,244,1)',
          borderWidth: 2,
          pointHitRadius: 0,
          pointRadius: 0,
          data: sumTeam1.map(e => ({
              x: e.x,
              y: e.y
          }))
        }, {
          label: data.team2.name,
          steppedLine: 'before',
          fill: true,
          backgroundColor: 'rgba(219,68,55,0.3)',
          borderColor: 'rgba(219,68,55,1)',
          borderWidth: 2,
          pointHitRadius: 0,
          pointRadius: 0,
          data: sumTeam2.map(e => ({
            x: e.x,
            y: e.y
          }))
        }]
      },
      options: {
        plugins: {
          datalabels: {
            display: false
          }
        },
        tooltips: {
          titleMarginBottom: 0,
          callbacks: {
            title: (tooltipItem) => {
              if (tooltipItem[0].datasetIndex === 0) {
                return sumTeam1.find(e => e.x === tooltipItem[0].xLabel && e.y === tooltipItem[0].yLabel)!.label!;
              } else if (tooltipItem[0].datasetIndex === 1) {
                return sumTeam2.find(e => e.x === tooltipItem[0].xLabel && e.y === tooltipItem[0].yLabel)!.label!;
              }
              return '';
            },
            label: () => ''
          }
        },
        legend: {
          onClick: () => {},
          labels: {
            filter: (legendItem) => {
              return legendItem.text !== undefined;
            }
          }
        },
        animation: {
          duration: 0
        },
        hover: {
          mode: 'point',
          animationDuration: 0,
        },
        responsiveAnimationDuration: 0,
        responsive: false,
        scales: {
          xAxes: [{
            type: 'linear',
            gridLines: {
              display: false
            },
            ticks: {
              stepSize: 10,
              max: maxMinute,
              callback: (value) => typeof value === 'number' && value % 5 === 0 ? value : ''
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'NPxG'
            }
          }]
        }
      }
    });
  }
}