import Chart from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { setCanvasDimensions, sum } from "../lib/helper";
import { Teams } from "../lib/teams";
import { Match } from "../lib/types";

type TeamStats = {
    team: keyof typeof Teams,
    points: number,
    xpoints: number,
}

export class XpointsToPointsPlot {
    constructor(selector: string, matches: Array<Match>) {
        const canvas: HTMLCanvasElement | null = document.querySelector(selector);
        if (!canvas) {
            window.console.error(`no canvas with selector ${selector} found`);
            return;
        }

        setCanvasDimensions(canvas, 3/2);

        const teams = matches
            .map(match => [match.teams.home, match.teams.away])
            .reduce((acc, val) => acc = acc.concat(...val), []);
        const uniqueTeams = teams.filter((e, i) => teams.indexOf(e) === i);
        const teamStats: Array<TeamStats> = uniqueTeams.map(team => {
            const teamMatches = matches.filter(match => match.teams.home === team || match.teams.away === team);
            const points = sum(teamMatches.map(match => {
                if (match.teams.home === team) {
                    if (match.result.home > match.result.away) {
                        return 3;
                    } else if (match.result.home === match.result.away) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    if (match.result.away > match.result.home) {
                        return 3;
                    } else if (match.result.home === match.result.away) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }));
            const xpoints = sum(teamMatches.map(match => match.teams.home === team ? match.xP.home : match.xP.away));
            return {
                team,
                points,
                xpoints,
            };
        });

        const canvasContext = canvas.getContext('2d');
        new Chart(canvasContext!, {
            plugins: [ChartDataLabels],
            data: {
                datasets: [{
                    type: 'scatter',
                    fill: false,
                    backgroundColor: 'rgba(66,133,244,0.3)',
                    hoverBackgroundColor: 'rgba(66,133,244,0.3)',
                    borderColor: 'rgba(66,133,244,1)',
                    hoverBorderColor: 'rgba(66,133,244,1)',
                    borderWidth: 1,
                    pointHitRadius: 5,
                    pointRadius: 5,
                    pointHoverRadius: 5,
                    showLine: false,
                    data: teamStats.map(s => ({
                        x: s.xpoints,
                        y: s.points - s.xpoints
                    }))
                }, {
                    type: 'line',
                    fill: false,
                    backgroundColor: 'rgba(255,102,102,1)',
                    borderColor: 'rgba(255,102,102,1)',
                    borderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 0,
                    data: [{
                        x: Math.floor(Math.min(...teamStats.map(t => t.xpoints)) / 10) * 10,
                        y: 0
                    }, {
                        x: Math.ceil(Math.max(...teamStats.map(t => t.xpoints)) / 10) * 10,
                        y: 0
                    }]
                }]
            },
            options: {
                plugins: {
                    datalabels: {
                        anchor: 'end',
                        align: 270,
                        offset: -4,
                        formatter: (_, context) => {
                            if (context.dataset.type === 'scatter') {
                                return Teams[teamStats[context.dataIndex].team];
                            }
                            return '';
                        }
                    }
                },
                tooltips: {
                    enabled: false
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 0
                },
                hover: {
                    mode: 'point',
                    animationDuration: 0,
                },
                responsiveAnimationDuration: 0,
                responsive: false,
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        scaleLabel: {
                            display: true,
                            labelString: 'Expected Points'
                        },
                        ticks: {
                            min: Math.floor(Math.min(...teamStats.map(t => t.xpoints)) / 10) * 10,
                            max: Math.ceil(Math.max(...teamStats.map(t => t.xpoints)) / 10) * 10
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Points - Expected Points'
                        },
                        ticks: {
                            min: Math.floor(Math.min(...teamStats.map(t => t.points - t.xpoints)) / 10) * 10,
                            max: Math.ceil(Math.max(...teamStats.map(t => t.points - t.xpoints)) / 10) * 10
                        }
                    }]
                }
            }
        });
    }
}