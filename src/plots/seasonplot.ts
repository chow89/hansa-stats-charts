import { Match } from "../lib/types";
import { Teams } from "../lib/teams";
import { predict, getTeamColor, sumByPointDimension, setCanvasDimensions } from "../lib/helper";
import Chart from "chart.js";

type SeasonPlotMatch = {
    team: Teams,
    points: number,
    xp: number
};

export class SeasonPlot {
    private chart: Chart | undefined = undefined;

    constructor(selector: string, matches: Array<Match>, appendTable: boolean = false) {
        const canvas: HTMLCanvasElement | null = document.querySelector(selector);
        if (!canvas) {
            window.console.error(`no canvas with selector ${selector} found`);
            return;
        }

        const dimensions = setCanvasDimensions(canvas, 9/8);

        const convertedMatches: Array<SeasonPlotMatch> = matches.map((match) => [{
            team: Teams[match.teams.home],
            points: match.result.home > match.result.away ? 3 : match.result.home === match.result.away ? 1 : 0,
            xp: match.xP.home
        }, {
            team: Teams[match.teams.away],
            points: match.result.away > match.result.home ? 3 : match.result.home === match.result.away ? 1 : 0,
            xp: match.xP.away
        }]
        ).reduce((acc, val) => acc = acc.concat(...val), []);

        const teams = convertedMatches.map((e: any) => e.team);
        const uniqueTeams = teams.filter((e, i) => teams.indexOf(e) === i);
        const matchesByTeam = uniqueTeams.map((team) => convertedMatches.filter(e => e.team === team));

        const avgPointsPerTeam: Array<{team: string, points: number}> = [];
        const datasets = matchesByTeam.map((team) => {
            const points = team.map(e => e.points);
            const expectedPoints = team.map(e => e.xp);
            const expectedPointsPrediction = predict(expectedPoints, 38);
            const name = team[0].team;
            avgPointsPerTeam.push({
                team: name,
                points: 0.5 * (expectedPointsPrediction.min - expectedPoints.reduce((a, b) => a + b) + points.reduce((a, b) => a + b) + expectedPointsPrediction.max - expectedPoints.reduce((a, b) => a + b) + points.reduce((a, b) => a + b))
            });
            return [{
                label: name,
                type: 'line',
                borderWidth: 2,
                borderColor: getTeamColor(name).toString(),
                pointRadius: 1,
                pointHoverRadius: 3,
                fill: false,
                lineTension: 0,
                data: sumByPointDimension(points.map((e: number, i: number) => {
                    return {
                        x: i + 1,
                        y: e
                    }
                }))
            }, {
                type: 'line',
                borderWidth: 2,
                borderColor: getTeamColor(name).withAlpha(0.3).toString(),
                pointRadius: 0,
                pointHoverRadius: 0,
                fill: false,
                lineTension: 0,
                data: [{
                    x: points.length,
                    y: points.reduce((a, b) => a + b)
                }, {
                    x: 38,
                    y: expectedPointsPrediction.min - expectedPoints.reduce((a, b) => a + b) + points.reduce((a, b) => a + b)
                }]
            }, {
                type: 'line',
                borderWidth: 2,
                borderColor: getTeamColor(name).withAlpha(0.3).toString(),
                pointRadius: 0,
                pointHoverRadius: 0,
                fill: false,
                lineTension: 0,
                data: [{
                    x: points.length,
                    y: points.reduce((a, b) => a + b)
                }, {
                    x: 38,
                    y: expectedPointsPrediction.max - expectedPoints.reduce((a, b) => a + b) + points.reduce((a, b) => a + b)
                }]
            }]
        }).reduce((acc, val) => acc = acc.concat(...val), []);;

        const canvasContext = canvas.getContext('2d');
        this.chart = new Chart(canvasContext!, {
            type: 'line',
            data: {
                datasets
            },
            options: {
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                tooltips: {
                    enabled: true,
                    titleMarginBottom: 0,
                    callbacks: {
                        title: () => '',
                    }
                },
                legend: {
                    display: dimensions.height >= 480,
                    onClick: (_, legendItem) => {
                        const datasetIndex = legendItem.datasetIndex!;
                        const chart = this.chart!;

                        [
                            chart.getDatasetMeta(datasetIndex),
                            chart.getDatasetMeta(datasetIndex + 1),
                            chart.getDatasetMeta(datasetIndex + 2)
                        ].forEach(meta => {
                            if (meta.hidden === undefined || meta.hidden === null) {
                                meta.hidden = !chart.data.datasets![datasetIndex].hidden;
                            } else {
                                meta.hidden = undefined;
                            }
                        });

                        chart.update();
                    },
                    labels: {
                        boxWidth: 12,
                        filter: (legendItem) => {
                            return legendItem.text !== undefined;
                        }
                    }
                },
                animation: {
                    duration: 0
                },
                responsiveAnimationDuration: 0,
                responsive: false,
                scales: {
                    xAxes: [{
                        type: 'linear',
                        ticks: {
                            stepSize: 2,
                            min: 1,
                            max: 38,
                            callback: (value) => typeof value === 'number' && value % 2 === 0 ? value : ''
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Spieltag'
                          }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Kumulierte Punkte'
                          }
                    }]
                }
            }
        });

        if (appendTable) {
            const table = document.createElement('table');
            const thead = document.createElement('thead');
            table.appendChild(thead);
            const tr = document.createElement('tr');
            thead.appendChild(tr);
            ['Platz', 'Verein', 'Punkte'].forEach(e => {
                const cell = document.createElement('th');
                cell.innerText = e;
                tr.appendChild(cell);
            });
            const tbody = document.createElement('tbody');
            table.appendChild(tbody);
            avgPointsPerTeam.sort((a, b) => b.points - a.points).forEach((e, i) => {
                var row = document.createElement('tr');

                var positionCell = document.createElement('td');
                positionCell.innerText = (i + 1).toString();
                row.appendChild(positionCell);

                var teamCell = document.createElement('td');
                teamCell.innerText = e.team;
                row.appendChild(teamCell);

                var pointsCell = document.createElement('td');
                pointsCell.innerText = e.points.toFixed(2);
                row.appendChild(pointsCell);

                tbody.appendChild(row);
            });

            canvas.parentNode?.insertBefore(table, canvas.nextSibling);
        }
    }
}