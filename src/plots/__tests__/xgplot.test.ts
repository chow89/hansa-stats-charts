import Chart from 'chart.js';
import { XgPlot, XgPlotData } from '../xgplot';

jest.mock('chart.js', () => {
    return jest.fn().mockImplementation(() => {
        return {};
    });
});

describe('XgPlot', () => {
    let canvas: HTMLCanvasElement | undefined;
    const canvasId = 'canvas';
    const inputData: XgPlotData = {
        team1: {
          name: 'Team A',
          points: [[2, 0.4], [10, 0.2], [10, 0.1, 'Tor']]
        },
        team2: {
          name: 'Team B',
          points: [[21, 0.1], [47, 0.05]]
        }
    };

    beforeEach(() => {
        canvas = window.document.createElement('canvas');
        canvas.setAttribute('id', canvasId);
        document.body.appendChild(canvas);
    });

    afterEach(() => {
        canvas = undefined;
        document.querySelector(`#${canvasId}`)?.remove();
    });

    it('calls ChartJs with aggregated data', () => {
        new XgPlot(`#${canvasId}`, inputData);
        expect(Chart).toHaveBeenCalledWith(expect.anything(), expect.objectContaining({
            type: 'line',
            data: {
                datasets: expect.arrayContaining([
                    expect.objectContaining({
                        data: [{
                            x: 0,
                            y: 0
                        }, {
                            x: 2,
                            y: 0.4
                        }, {
                            x: 10,
                            y: 0.6
                        }, {
                            x: 10,
                            y: 0.7
                        }, {
                            x: 91,
                            y: 0.7
                        }],
                        label: 'Team A'
                    }),
                    expect.objectContaining({
                        data: [{
                            x: 0,
                            y: 0
                        }, {
                            x: 21,
                            y: 0.1
                        }, {
                            x: 47,
                            y: 0.15
                        }, {
                            x: 91,
                            y: 0.15
                        }],
                        label: 'Team B'
                    }),
                ])
            }
        }));
    });

    it('calls ChartJs with goals marker data', () => {
        new XgPlot(`#${canvasId}`, inputData);
        expect(Chart).toHaveBeenCalledWith(expect.anything(), expect.objectContaining({
            data: {
                datasets: expect.arrayContaining([
                    expect.objectContaining({
                        type: 'scatter',
                        data: [{
                            x: 10,
                            y: 0.7
                        }]
                    })
                ])
            }
        }));
    });

    it('calls ChartJs with options', () => {
        new XgPlot(`#${canvasId}`, inputData);
        expect(Chart).toHaveBeenCalledWith(expect.anything(), expect.objectContaining({
            options: expect.objectContaining({
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: expect.objectContaining({
                            max: 91,
                            stepSize: 10
                        }),
                        type: 'linear'
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'NPxG'
                        }
                    }]
                }
            })
        }));
    });
});