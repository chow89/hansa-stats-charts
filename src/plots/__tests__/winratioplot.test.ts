import Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { ResultProbabilityMatrix, WinRatioPlot } from '../winratioplot';

jest.mock('chart.js', () => {
    return jest.fn().mockImplementation(() => {
        return {};
    });
});

jest.mock('chartjs-plugin-datalabels', () => jest.fn());

describe('WinRatioPlot', () => {
    let canvas: HTMLCanvasElement | undefined;
    const canvasId = 'canvas';
    const inputData: ResultProbabilityMatrix = [[.2, .15], [.3, .35]];

    beforeEach(() => {
        canvas = window.document.createElement('canvas');
        canvas.setAttribute('id', canvasId);
        document.body.appendChild(canvas);
    });

    afterEach(() => {
        canvas = undefined;
        document.querySelector(`#${canvasId}`)?.remove();
    });

    it('calls ChartJs with aggregated data', () => {
        new WinRatioPlot(`#${canvasId}`, inputData);
        expect(Chart).toHaveBeenCalledWith(expect.anything(), expect.objectContaining({
            type: 'horizontalBar',
            plugins: [ChartDataLabels],
            data: {
                datasets: [
                    expect.objectContaining({
                        data: [30]
                    }),
                    expect.objectContaining({
                        data: [55]
                    }),
                    expect.objectContaining({
                        data: [15]
                    })
                ]
            }
        }));
    });

    it('calls ChartJs with options', () => {
        new WinRatioPlot(`#${canvasId}`, inputData);
        expect(Chart).toHaveBeenCalledWith(expect.anything(), expect.objectContaining({
            options: expect.objectContaining({
                plugins: {
                    datalabels: expect.objectContaining({
                        display: expect.any(Function),
                        formatter: expect.any(Function)
                    })
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 0
                },
                hover: {
                    animationDuration: 0
                },
                tooltips: {
                    enabled: false
                },
                responsiveAnimationDuration: 0,
                responsive: false,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        ticks: {
                            max: 100
                        }
                    }],
                    yAxes: [{
                        display: false,
                        stacked: true
                    }]
                }
            })
        }));
    });
});