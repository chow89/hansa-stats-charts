import Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { cumulateProbabilitiesByResult, round, setCanvasDimensions } from '../lib/helper';
import { Logger } from '../lib/logger';

export type ResultProbabilityMatrix = Array<Array<number>>;
export type OutcomeProbabilityTriple = [number, number, number];

const isResultProbabilityMatrix = (data: ResultProbabilityMatrix | OutcomeProbabilityTriple): data is ResultProbabilityMatrix => {
    return Array.isArray(data[0]);
};

export class WinRatioPlot {
    constructor(selector: string, data: ResultProbabilityMatrix | OutcomeProbabilityTriple) {
        const canvas: HTMLCanvasElement | null = document.querySelector(selector);
        if (!canvas) {
            window.console.error(`no canvas with selector ${selector} found`);
            return;
        }

        const dimensions = setCanvasDimensions(canvas, 24);

        let win, draw, loss;
        if (isResultProbabilityMatrix(data)) {
            [ win, draw, loss ] = cumulateProbabilitiesByResult(data);
        } else {
            [ win, draw, loss ] = data;
        }

        Logger.log('Probabilities', win, draw, loss);
        Logger.log('Expected Points', 3 * win + draw);
        Logger.log('Expected Points Against', 3 * loss + draw);

        const canvasContext = canvas.getContext('2d');
        new Chart(canvasContext!, {
            plugins: [ChartDataLabels],
            type: 'horizontalBar',
            data: {
                datasets: [{
                    data: [round(win * 100, 2)],
                    barThickness: 30,
                    backgroundColor: 'rgba(66,133,244,1)',
                    hoverBackgroundColor: 'rgba(66,133,244,1)'
                }, {
                    data: [round(draw * 100, 2)],
                    barThickness: 30,
                    backgroundColor: 'rgba(244,180,0,1)',
                    hoverBackgroundColor: 'rgba(244,180,0,1)'
                }, {
                    data: [round(loss * 100, 2)],
                    barThickness: 30,
                    backgroundColor: 'rgba(219,68,55,1)',
                    hoverBackgroundColor: 'rgba(219,68,55,1)'
                }]
            },
            options: {
                plugins: {
                    datalabels: {
                        color: '#ffffff',
                        font: {
                            weight: 'bold',
                            size: dimensions.height < 16 ? 10 : 12
                        },
                        display: (context) => {
                            return context.dataset.data![context.dataIndex]! >= 8;
                        },
                        formatter: (value) => {
                            return value + '%';
                        }
                    }
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 0
                },
                hover: {
                    animationDuration: 0,
                },
                tooltips: {
                    enabled: false
                },
                responsiveAnimationDuration: 0,
                responsive: false,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                        ticks: {
                            max: 100
                        }
                    }],
                    yAxes: [{
                        display: false,
                        stacked: true
                    }]
                }
            }
        });
    }
}