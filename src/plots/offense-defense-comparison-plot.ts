import { Match } from "../lib/types";
import { average, setCanvasDimensions } from "../lib/helper";
import Chart from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Teams } from "../lib/teams";

type TeamStats = {
    team: keyof typeof Teams,
    opponents: Array<string>,
    xg90: number,
    xga90: number,
    opponentXgs?: number,
    opponentXgas?: number,
    offense?: number,
    defense?: number
}

export class OffenseDefenseComparisonPlot {
    constructor(selector: string, matches: Array<Match>) {
        const canvas: HTMLCanvasElement | null = document.querySelector(selector);
        if (!canvas) {
            window.console.error(`no canvas with selector ${selector} found`);
            return;
        }

        setCanvasDimensions(canvas, 3/2);

        const teams = matches
            .map(match => [match.teams.home, match.teams.away])
            .reduce((acc, val) => acc = acc.concat(...val), []);
        const uniqueTeams = teams.filter((e, i) => teams.indexOf(e) === i);
        let teamStats: Array<TeamStats> = uniqueTeams.map(team => {
            const teamMatches = matches.filter(match => match.teams.home === team || match.teams.away === team);
            const xGs = teamMatches.map(match => match.teams.home === team ? match.xG.home : match.xG.away);
            const xGAs = teamMatches.map(match => match.teams.home === team ? match.xG.away : match.xG.home);
            const opponents = teamMatches.map(match => match.teams.home === team ? match.teams.away : match.teams.home);
            return {
                team,
                opponents,
                xg90: average(xGs),
                xga90: average(xGAs)
            };
        });
        teamStats = teamStats.map(team => {
            const opponentXgs = team.opponents.map(opponent => ({
                xg: teamStats.find(t => t.team === opponent)!.xg90,
                xga: teamStats.find(t => t.team === opponent)!.xga90
            }));
            return {
                ...team,
                opponentXgs: average(opponentXgs.map(x => x.xg)),
                opponentXgas: average(opponentXgs.map(x => x.xga))
            }
        }).map(team => ({
            ...team,
            offense: team.xg90 / team.opponentXgas! - 1,
            defense: -team.xga90 / team.opponentXgs! + 1
        }));

        const canvasContext = canvas.getContext('2d');
        new Chart(canvasContext!, {
            plugins: [ChartDataLabels],
            data: {
                datasets: [{
                    type: 'scatter',
                    fill: false,
                    backgroundColor: 'rgba(66,133,244,0.3)',
                    hoverBackgroundColor: 'rgba(66,133,244,0.3)',
                    borderColor: 'rgba(66,133,244,1)',
                    hoverBorderColor: 'rgba(66,133,244,1)',
                    borderWidth: 1,
                    pointHitRadius: 5,
                    pointRadius: 5,
                    pointHoverRadius: 5,
                    showLine: false,
                    data: teamStats.map(s => ({
                        x: s.offense,
                        y: s.defense
                    }))
                }, {
                    type: 'line',
                    fill: false,
                    backgroundColor: 'rgba(255,102,102,1)',
                    borderColor: 'rgba(255,102,102,1)',
                    borderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 0,
                    data: [{
                        x: 0,
                        y: -1
                    }, {
                        x: 0,
                        y: 1
                    }]
                }, {
                    type: 'line',
                    fill: false,
                    backgroundColor: 'rgba(255,102,102,1)',
                    borderColor: 'rgba(255,102,102,1)',
                    borderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 0,
                    data: [{
                        x: -1,
                        y: 0
                    }, {
                        x: 1,
                        y: 0
                    }]
                }]
            },
            options: {
                plugins: {
                    datalabels: {
                        anchor: 'end',
                        align: 270,
                        offset: -4,
                        formatter: (_, context) => {
                            if (context.dataset.type === 'scatter') {
                                return Teams[teamStats[context.dataIndex].team];
                            }
                            return '';
                        }
                    }
                },
                tooltips: {
                    enabled: false
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 0
                },
                hover: {
                    mode: 'point',
                    animationDuration: 0,
                },
                responsiveAnimationDuration: 0,
                responsive: false,
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        scaleLabel: {
                            display: true,
                            labelString: 'Offensive'
                        },
                        ticks: {
                            min: Math.floor(10 * Math.min(...teamStats.map(t => t.offense!))) / 10,
                            max: Math.ceil(10 * Math.max(...teamStats.map(t => t.offense!))) / 10
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Defensive'
                        },
                        ticks: {
                            min: Math.floor(10 * Math.min(...teamStats.map(t => t.defense!))) / 10,
                            max: Math.ceil(10 * Math.max(...teamStats.map(t => t.defense!))) / 10
                        }
                    }]
                }
            }
        });
    }
}