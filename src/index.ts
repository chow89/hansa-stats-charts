import { XgPlot } from './plots/xgplot';
import { WinRatioPlot } from './plots/winratioplot';
import { ResultProbabilityCalculator } from './lib/result-probability-calculator';
import { Teams } from './lib/teams';
import { JusticeTable } from './lib/justice-table';
import { SeasonPlot } from './plots/seasonplot';
import { OffenseDefenseComparisonPlot } from './plots/offense-defense-comparison-plot';
import { GsaaToWorkloadPlot } from './plots/gsaa-to-workload-plot';
import { XpointsToPointsPlot } from './plots/xpoints-to-points-plot';

(window as any).HansaStatsChart = {
    XgPlot: XgPlot,
    WinRatioPlot: WinRatioPlot,
    ResultProbabilityCalculator: ResultProbabilityCalculator,
    Teams: Teams,
    JusticeTable: JusticeTable,
    SeasonPlot: SeasonPlot,
    OffenseDefenseComparisonPlot: OffenseDefenseComparisonPlot,
    GsaaToWorkloadPlot: GsaaToWorkloadPlot,
    XpointsToPointsPlot: XpointsToPointsPlot
};