import { ResultProbabilityCalculator } from "../result-probability-calculator";

describe('ResultProbabilityCaclculator', () => {
    test('.getResultProbabilities() creates a result matrix with all result probabilities', () => {
        const team1 = [.5, .5, .5, .5];
        const team2 = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6, 1/6];

        const resultMatrix = new ResultProbabilityCalculator().getResultProbabilities(team1, team2);

        expect(resultMatrix.length).toEqual(team1.length + 1);
        expect(resultMatrix[0].length).toEqual(team2.length + 1);
        expect(resultMatrix[1][0]).toBeGreaterThan(resultMatrix[0][1]);
    });
});