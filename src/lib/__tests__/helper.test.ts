import { standardDeviation, variance } from "../helper";

describe('helper', () => {
    test('standardDeviation', () => {
        expect(standardDeviation([49,51,58,53,52,48,51,56])).toBeCloseTo(3.15);
    });

    test('variance', () => {
        expect(variance([49,51,58,53,52,48,51,56])).toBeCloseTo(9.94);
    });
});