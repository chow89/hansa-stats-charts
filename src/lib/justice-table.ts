import { Match } from "./types";
import { TableEntry } from "../adt/table-entry";
import { Teams } from "./teams";

export class JusticeTable {
    private entries: Array<TableEntry> = [];
    constructor(selector: string, matches: Array<Match>) {
        const table: HTMLTableElement | null = document.querySelector(selector);
        if (!table) {
            window.console.error(`no table with selector ${selector} found`);
            return;
        }
        for (let match of matches) {
            this.addMatchToTable(match, 'home');
            this.addMatchToTable(match, 'away');
        }
        this.addTableHead(table);
        this.addTableBody(table, this.getEntries());
    }

    private addTableHead(table: HTMLTableElement) {
        const columnNames = ['Platz', 'Verein', 'Spiele', 'NPxG', 'NPxGA', 'xP'];
        table.innerHTML = '<thead><tr>' + columnNames.map(e => '<th>' + e + '</th>').join('') + '</tr></thead>';
    }

    private addTableBody(table: HTMLTableElement, entries: Array<TableEntry>) {
        const tbody = document.createElement('tbody');
        entries.forEach((e, i) => {
            var row = document.createElement('tr');

            var positionCell = document.createElement('td');
            positionCell.innerText = (i + 1).toString();
            row.appendChild(positionCell);

            var teamCell = document.createElement('td');
            teamCell.innerText = e.team;
            row.appendChild(teamCell);

            var matchesCell = document.createElement('td');
            matchesCell.innerText = e.numberOfMatches.toString();
            row.appendChild(matchesCell);

            var xgCell = document.createElement('td');
            xgCell.innerHTML = e.totalExpectedGoals.toFixed(2)
            row.appendChild(xgCell);

            var xgaCell = document.createElement('td');
            xgaCell.innerHTML = e.totalExpectedGoalsAgainst.toFixed(2);
            row.appendChild(xgaCell);

            var xpCell = document.createElement('td');
            var xp = Math.round(e.totalExpectedPoints * 100) / 100;
            var diff = Math.round((xp - e.totalPoints) * 100) / 100;
            var diffColor = diff >= 0 ? 'red' : 'green';
            var diffSpan = document.createElement('span');
            diffSpan.style.fontSize = '12px';
            diffSpan.style.color = diffColor;
            diffSpan.innerHTML = `${(diff > 0 ? ' +' : ' ')}${diff.toFixed(2)}`;
            xpCell.innerHTML = e.totalExpectedPoints.toFixed(2);
            xpCell.appendChild(diffSpan);
            row.appendChild(xpCell);

            tbody.appendChild(row);
            table.appendChild(tbody);
        });
    }

    private getEntries() {
        return this.entries.sort((a, b) => b.totalExpectedPoints - a.totalExpectedPoints);
    }

    private addMatchToTable(match: Match, key: 'home' | 'away') {
        const contraryKey = key === 'home' ? 'away' : 'home';
        let entry = this.entries.find((e) => e.team === Teams[match.teams[key]]);
        if (!entry) {
            entry = new TableEntry(Teams[match.teams[key]]);
            this.entries.push(entry);
        }
        entry.addPoints(match.result[key] > match.result[contraryKey] ? 3 : match.result[key] === match.result[contraryKey] ? 1 : 0);
        entry.addGoals(match.result[key]);
        entry.addGoalsAgainst(match.result[contraryKey]);
        entry.addExpectedPoints(match.xP[key]);
        entry.addExpectedGoals(match.xG[key]);
        entry.addExpectedGoalsAgainst(match.xG[contraryKey]);
        entry.incrementMatchCounter();
    }
}