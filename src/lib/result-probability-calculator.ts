import { fillToLength, standardDeviation } from "./helper";
import { ResultProbabilityMatrix } from "../plots/winratioplot";
import { Logger } from "./logger";

export class ResultProbabilityCalculator {

    public getResultProbabilities(team1: Array<number>, team2: Array<number>): ResultProbabilityMatrix {
        Logger.log('total xG', team1.reduce((a, b) => a + b), 'Standardabweichung', standardDeviation(team1));
        Logger.log('total xGA', team2.reduce((a, b) => a + b), 'Standardabweichung', standardDeviation(team2));
        const probabilitiesTeam1 = this.calculateGoalProbabilities(team1);
        const probabilitiesTeam2 = this.calculateGoalProbabilities(team2);
        const expectedResults: ResultProbabilityMatrix = []
        for (let entryHome of probabilitiesTeam1) {
            for (let entryAway of probabilitiesTeam2) {
                if (!expectedResults[entryHome.goals]) {
                    expectedResults[entryHome.goals] = []
                }
                expectedResults[entryHome.goals][entryAway.goals] = entryHome.probability * entryAway.probability;
            }
        }
        Logger.log('Expected Results', expectedResults);
        return expectedResults;
    }

    private calculateGoalProbabilities(data: Array<number>): Array<GoalsProbability> {
        if (data.length === 0) {
            return [];
        }
        const pathProbabilities = [];
        for (let i = 0; i < 2 ** data.length; i++) {
            const asBinary = fillToLength(i.toString(2), data.length);
            let probability = 1;
            for (let j = 0; j < asBinary.length; j++) {
                probability *= asBinary[j] === "1" ? data[j] : 1 - data[j];
            }
            pathProbabilities.push({
                probability: probability,
                goals: asBinary.split('').reduce((acc: number, cur: string) => acc + parseInt(cur), 0)
            });
        }
        return this.groupByGoals(pathProbabilities);
    }

    private groupByGoals(probabilities: Array<GoalsProbability>): Array<GoalsProbability> {
        const buckets: Array<Array<GoalsProbability>> = [];
        for (let entry of probabilities) {
            if (!buckets[entry.goals]) {
                buckets[entry.goals] = [];
            }
            buckets[entry.goals].push(entry);
        }
        const summedProbabilities: Array<GoalsProbability> = [];
        for (let bucket of buckets) {
            summedProbabilities.push({
                probability: bucket.reduce((acc, cur) => acc + cur.probability, 0),
                goals: bucket[0].goals
            });
        }
        return summedProbabilities;
    }

}

export type GoalsProbability = {
    probability: number;    // TODO: refine to probability, which is number in [0,1]
    goals: number;
}
