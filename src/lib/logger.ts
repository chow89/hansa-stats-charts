export class Logger {
    public static log(...s: Array<any>) {
        if (sessionStorage.getItem('charts-additional-logging') === '1') {
            window.console.log(...s);
        }
    }
}