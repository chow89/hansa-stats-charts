import { Teams } from "./teams";

export type Match = {
    teams: {
        home: keyof typeof Teams,
        away: keyof typeof Teams
    },
    result: {
        home: number,
        away: number
    },
    xG: {
        home: number,
        away: number
    }
    xP: {
        home: number,
        away: number
    },
    matchday: number
}

export type Player = {
    name: string,
    dateOfBirth?: Date,
    playtime?: number,
    team: keyof typeof Teams
  }
  
  export type Goalkeeper = Player & {
    totalShotsAgainst: number;
    totalGoalsConceded: number;
    totalExpectedGoalsConceded: number;
  }