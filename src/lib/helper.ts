import { Point } from "chart.js"
import { ResultProbabilityMatrix } from "../plots/winratioplot";
import { Goalkeeper } from "./types";
import { Teams } from "./teams";

export const sumByPointDimension = (data: Array<Point & { label?: string }>) => {
  const result: Array<Point & { label?: string }> = [];
  data.forEach(point => {
    if (result.length === 0) {
      result.push(point);
    } else {
      result.push({
        x: point.x,
        y: round(point.y + result[result.length - 1].y, 2),
        label: point.label
      });
    }
  });
  return result;
};

export const round = (x: number, precision: number) => Math.round(x * (10 ** precision)) / (10 ** precision);

export const cumulateProbabilitiesByResult = (resultMatrix: ResultProbabilityMatrix) => {
  let win = 0;
  let draw = 0;
  let loss = 0;
  for (let i = 0; i < resultMatrix.length; i++) {
    for (let j = 0; j < resultMatrix[i].length; j++) {
      if (i > j) {
        win += resultMatrix[i][j];
      } else if (i < j) {
        loss += resultMatrix[i][j];
      } else {
        draw += resultMatrix[i][j];
      }
    }
  }
  return [ win, draw, loss ];
}

export const fillToLength = (s: string, length: number): string => {
  let filledString = s;
  for (let i = 0; i < length - s.length; i++) {
    filledString = '0' + filledString;
  }
  return filledString;
}

export const factorial = (n: number): number => n === 0 || n === 1 ? 1 : n * factorial(n - 1);

export const poisson = (lambda: number, k: number): number => (Math.exp(-lambda) * Math.pow(lambda, k)) / factorial(k);

export const sum = (ns: Array<number>) => ns.reduce((a, b) => a + b);

export const average = (ns: Array<number>) => sum(ns) / ns.length;

export const standardDeviation = (n: Array<number>) => {
  const average = n.reduce((a, b) => a + b) / n.length;
  const squaredDifferences = n.map((elem) => (elem - average) ** 2);
  const summedDifferences = squaredDifferences.reduce((a, b) => a + b);
  return Math.sqrt(summedDifferences / n.length)
}

export const variance = (n: Array<number>) => standardDeviation(n) ** 2;

export const calculateGsaa = (p: Goalkeeper) => {
  return (p.totalShotsAgainst - p.totalGoalsConceded) / p.totalShotsAgainst - (p.totalShotsAgainst - p.totalExpectedGoalsConceded) / p.totalShotsAgainst;
}

export const predict = (a: Array<number>, elementToPredict: number) => {
  const sum = a.reduce((a, b) => a + b);
  const average = sum / a.length;
  const deviation = standardDeviation(a);
  const numberOfElementsToPredict = elementToPredict - a.length;
  return {
    min: sum + numberOfElementsToPredict * (average - deviation),
    max: sum + numberOfElementsToPredict * (average + deviation)
  }
}

export const getTeamColor = (team: Teams) => {
  switch (team) {
    case Teams.MSV_DUISBURG:
    case Teams.FC_MAGDEBURG:
    case Teams.HANSA_ROSTOCK:
    case Teams.SV_MEPPEN:
    case Teams.CHEMNITZER_FC:
    case Teams.KFC_UERDINGEN:
    case Teams.TSV_1860_MÜNCHEN:
    case Teams.WALDHOF_MANNHEIM:
    case Teams.FC_SAARBRÜCKEN:
    case Teams.SC_PADERBORN:
    case Teams.SV_DARMSTADT:
    case Teams.KARLSRUHER_SC:
    case Teams.HAMBURGER_SV:
    case Teams.HOLSTEIN_KIEL:
    case Teams.SCHALKE_04:
      return new Color(66, 133, 244); // blue
    case Teams.HALLESCHER_FC:
    case Teams.WÜRZBURGER_KICKERS:
    case Teams.FSV_ZWICKAU:
    case Teams.FC_KAISERSLAUTERN:
    case Teams.BAYERN_MÜNCHEN_II:
    case Teams.FC_INGOLSTADT:
    case Teams.SPVGG_UNTERHACHING:
    case Teams.SONNENHOF_GROSSASPACH:
    case Teams.VIKTORIA_KÖLN:
    case Teams.WEHEN_WIESBADEN:
    case Teams.TÜRKGÜCÜ_MÜNCHEN:
    case Teams.JAHN_REGENSBURG:
    case Teams.HEIDENHEIM:
    case Teams.FORTUNA_DÜSSELDORF:
    case Teams.FC_KÖLN:
      return new Color(219, 68, 55);  // red
    case Teams.CARL_ZEISS_JENA:
    case Teams.EINTRACHT_BRAUNSCHWEIG:
    case Teams.DYNAMO_DRESDEN:
      return new Color(244, 180, 0);  // yellow
    case Teams.PREUSSEN_MÜNSTER:
    case Teams.VFB_LÜBECK:
    case Teams.WERDER_BREMEN:
      return new Color(25, 124, 36);  // green
    case Teams.SC_VERL:
    case Teams.SV_SANDHAUSEN:
    case Teams.HANNOVER_96:
      return new Color(0, 0, 0);  // black
    case Teams.VFL_OSNABRÜCK:
    case Teams.ERZGEBIRGE_AUE:
      return new Color(80, 33, 141);  // purple
    case Teams.ST_PAULI:
      return new Color(227, 1, 11); // brown
    default:
      return new Color(0, 0, 0);
  }
}

export const setCanvasDimensions =(canvas: HTMLCanvasElement, aspectRatio: number) => {
  const parent = canvas.parentElement!;
  const leftPadding = parseInt(getComputedStyle(parent, undefined).getPropertyValue('padding-left').replace('px', ''));
  const rightPadding = parseInt(getComputedStyle(parent, undefined).getPropertyValue('padding-right').replace('px', ''));
  const width = parent.getBoundingClientRect().width - leftPadding - rightPadding
  canvas.setAttribute('width', width.toString());
  canvas.setAttribute('height', (width / aspectRatio).toString());

  return {
    width,
    height: width * aspectRatio
  }
}

export class Color {
  private r: number;
  private g: number;
  private b: number;
  private a: number;

  constructor(r: number, g: number, b: number, a?: number) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a || 1;
  }

  public withAlpha(a: number) {
    this.a = a;
    return this;
  }

  public toString() {
    return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
  }
}