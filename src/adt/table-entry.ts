import { Teams } from "../lib/teams";

export class TableEntry {
    public team: Teams;
    public totalPoints: number = 0;
    public totalExpectedPoints: number = 0;
    public totalGoals: number = 0;
    public totalGoalsAgainst: number = 0;
    public totalExpectedGoals: number = 0;
    public totalExpectedGoalsAgainst: number = 0;
    public numberOfMatches: number = 0;

    constructor(team: Teams, numberOfMatches = 0, points = 0, expectedPoints = 0, goals = 0, goalsAgainst = 0, expectedGoals = 0, expectedGoalsAgainst = 0) {
        this.team = team;
        this.numberOfMatches = numberOfMatches;
        this.totalPoints = points;
        this.totalExpectedPoints = expectedPoints;
        this.totalGoals = goals;
        this.totalGoalsAgainst = goalsAgainst;
        this.totalExpectedGoals = expectedGoals;
        this.totalExpectedGoalsAgainst = expectedGoalsAgainst;
    }

    public addPoints(points: number) {
        this.totalPoints += points;
    }

    public addExpectedPoints(points: number) {
        this.totalExpectedPoints += points;
    }

    public addGoals(goals: number) {
        this.totalGoals += goals;
    }

    public addGoalsAgainst(goals: number) {
        this.totalGoalsAgainst += goals;
    }

    public addExpectedGoals(goals: number) {
        this.totalExpectedGoals += goals;
    }

    public addExpectedGoalsAgainst(goals: number) {
        this.totalExpectedGoalsAgainst += goals;
    }

    public incrementMatchCounter() {
        this.numberOfMatches++;
    }
}